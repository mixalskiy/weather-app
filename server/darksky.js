module.exports = {

    getUrl: (params) => {
        return `http://api.openweathermap.org/data/2.5/forecast/daily?q=${params.q}&apikey=${params.apikey}&mode=${params.mode}&units=${params.units}&cnt=${params.cnt || 3}`
    },

    currentForecast: (json) => {
        const entries = {
            location: json.city,
            forecast: json.list
        };
        return entries;
    }

};
