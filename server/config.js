const packageJSON = require("./../package.json");

module.exports = {
    messages: {
        not_found: { message: "Not Found" },
        unauthorized: { message: "Unauthorized" },
        internal_server_error: { message: "Internal Server Error" },
    },
    application_name: packageJSON.name,
    server_port: process.env.PORT || 8000,
    application_version: packageJSON.version,
    location_log_size: process.env.LOCATION_LOG_SIZE || 10,
    location_precision: process.env.LOCATION_PRECISION || 2,
    api_key: process.env.API_KEY || "94b7c9a9908d65670675b7edd72079fc",
    secret_key: process.env.SECRET_KEY || "2a59495e89534508bf595ce5431c50a7",
    forecast_cache_duration: process.env.FORECAST_CACHE_DURATION || "6 hours",
};
