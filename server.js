const util = require("./server/util");
const helmet = require("helmet");
const express = require("express");
const request = require("request");
const config = require("./server/config");
const darksky = require("./server/darksky");
const usersCache = require("./server/users.cache");
const temporalCache = require("./server/temporal.cache");
const cookieParser = require('cookie-parser');

const app = express();
app.use(helmet());
app.use(cookieParser('1323defrw'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    next();
});

const middleware = {
    auth: function (req, res, next) {
        if (req.query.apikey === config.api_key) {
            return next();
        }
        return res.status(403).json(config.messages.unauthorized);
    }
}

app.post("/users", middleware.auth, (req, res) => {
    const users = usersCache.all();
    return res.status(200).json(users);
});

app.post("/cache/:location", middleware.auth, (req, res) => {

    const location = req.params.location;
    const data = temporalCache.get(location);
    if (data) {
        return res.status(200).json(data);
    }

    return res.status(200).json({ message: "Location not cached" });

});

app.post("/api/forecast", middleware.auth, (req, res) => {
    const location = req.query.q;

    let data = temporalCache.get(location);
    if (data) {
        return util.forecast({
            res: res,
            data: data,
            cached: true,
            location: location
        });
    }
    res.cookie(location, location, { maxAge: 10000000, httpOnly: true });
    const url = darksky.getUrl(req.query);
    const opts = { method: "GET", url: url, jar: true };
    request(opts, (error, response, body) => {
        if (error || response.statusCode !== 200) {
            return res.status(500).json(config.messages.internal_server_error);
        }
        return util.forecast({
            res: res,
            cached: false,
            location: location,
            data: JSON.parse(body)
        });

    });

});

app.get("/status", (req, res) => {
    return res.status(200).json({ name: config.application_name, version: config.application_version });
});

app.get("/api/cities", (req, res) => {
    const cities = Object.keys(req.cookies);
    const cookiesLength = Object.getOwnPropertyNames(req.cookies).length;

    if (cookiesLength >= 4) {
        const cookies = Object.keys(req.cookies).reverse();
        res.clearCookie(cookies[cookiesLength]);
        cookies.pop();
        for (let i = 0; i < 5; i ++) {
            res.cookie(cookies[i], cookies[i], { maxAge: 10000000, httpOnly: true });
        }
    }

    return cities ? res.status(200).json({ cities: cities }) : [];
});

app.get("/*", (req, res) => {
    return res.status(404).json(config.messages.not_found);
});

app.listen(config.server_port, () => {
    console.log(`App ready! Listening at port ${config.server_port}!`)
});
