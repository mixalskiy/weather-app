import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class WeatherService {
  constructor(private http: HttpClient) {
    console.log('Production', environment.production);
  }
  getWeatheritemsbyCity(): Observable<any> {
    return this.http.get(
      environment.baseUrl + 'cities'
    )
  }

  getWeatherForecast(cityName: string): Observable<any> {
    return this.http.post(
      environment.baseUrl +
      'forecast?q=' + cityName +
      '&mode=json&units=metric&cnt=3&' +
      'apikey=' + environment.apikey
    )
  }

  private extractData(res: any) {
    const body = res.json();
    return body.list || {};
  }

  private handleError(error: any) {
    let errMsg: string;
    errMsg = error.message ? error.message : error.toString();
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
