import { Component, OnInit } from '@angular/core';
import {WeatherService} from '../weather.service';

@Component({
  selector: 'app-weather-today',
  templateUrl: './weather-today.component.html',
  styleUrls: ['./weather-today.component.scss']
})
export class WeatherTodayComponent implements OnInit {

  public data: any;
  constructor(private service: WeatherService) { }

  ngOnInit() {


    this.service.getWeatheritemsbyCity()
        .subscribe((data) => {
          this.data = data;
        });
  }

}
