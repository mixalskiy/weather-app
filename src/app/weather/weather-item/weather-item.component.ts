import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'weather-item',
  templateUrl: './weather-item.component.html'
})
export class WeatherItemComponent {
  @Input('WeatherItem') weather: any;
  @Output() weatherItemChanges = new EventEmitter();

  selectedWeatherItem(event: Event) {
    var item = (<HTMLDivElement>event.target);
    console.log('Selected div element', item);
    this.weatherItemChanges.emit(item.innerHTML);
  }
}
